﻿using PeanutButter.TrayIcon;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Threading;
using System.Windows;
using System.Windows.Interop;

namespace Control_10_12_2019
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        const int DEFAULT_PERIOD = 1;

        TrayIcon trayIcon;
        ScreenCapture screenCapturer;
        DateTime previousScreenTime;
        Random random;
        Timer timer;
        Timer checkTimer;
        string userName;
        int period;
        bool screened = false;
        public MainWindow()
        {
            InitializeComponent();
            trayIcon = new TrayIcon();
            random = new Random();
            period = DEFAULT_PERIOD;
            previousScreenTime = DateTime.Now;
            checkTimer = new Timer(CheckTimer, null, 0, 1000);
            screenCapturer = new ScreenCapture();
            //screener = new Screener(DEFAULT_PERIOD);
            //autoEvent = new AutoResetEvent(false);
            userName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            userName = userName.Substring(5); //Для теста в ШАГе. По умолчанию там CORP/{userName}
            //timer = new Timer(MakeScreenshot, null, 0, DEFAULT_PERIOD);
        }

        private void SaveButtonClick(object sender, RoutedEventArgs e)
        {
            if (!int.TryParse(periodBox.Text, out int newPeriod))
            {
                MessageBox.Show("Ошибка! Неверный формат периода");
                return;
            }



            //HideToTaskbar();
            if (period != newPeriod)
            {
                period = newPeriod;
            }
        }

        private void CheckTimer(object state)
        {
            if (DateTime.Now >= previousScreenTime.AddMinutes(period))
                screened = false;

            if(!screened)
            {
                int newPeriod = random.Next(period);
                timer = new Timer(MakeScreenshot, null, 0, newPeriod);
                screened = true;
                previousScreenTime = DateTime.Now;
            }
        }

        private void HideToTaskbar()
        {
            trayIcon.DefaultBalloonTipTimeout = 5000;
            trayIcon.DefaultTipText = "Default tip text";
            trayIcon.DefaultTipTitle = "Default tip title";
            trayIcon.DefaultBalloonTipClickedAction = () => MessageBox.Show("You clicked the default balloon tip");
            trayIcon.Show();
        }
        private void MakeScreenshot(object state)
        {
            screenCapturer.CaptureScreenToFile($"C:\\Users\\{userName}\\Pictures\\{DateTime.Now.ToString("yyyy.MM.dd; HH.mm.ss")}.bmp", ImageFormat.Bmp);
        }

        private void WindowClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
            HideToTaskbar();
        }

        private void Window_Deactivated(object sender, EventArgs e)
        {
            if (this.WindowState == WindowState.Minimized)
            {
                this.ShowInTaskbar = false;
            }
        }
    }
}
